// 'use strict'
const mikeSlider = () => {

	const sliderSettings = {
		displayTitle: true,
		navButtons: true,
		buttonPrev: '<',
		buttonNext: '>',
		imagesPerSlide: 5,
		moveStep: 1,
		autoSlide: true,
		autoSlideTime: 5000
	};
	const imgs = {
		'title': 'Some images',
		'images': [
			{
				'imgUrl': 'img/banter-snaps-14425-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'What you think?',
				'description': 'Yes, looks like very nice shoe.'
			},
			{
				'imgUrl': 'img/ben-garratt-134771-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'Blue couch',
				'description': 'Blue comes in many shades, in this case we have blue couch.'
			},
			{
				'imgUrl': 'img/brandon-wilson-52835-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'Guitar',
				'description': 'The guitar is a fretted musical instrument that usually has six strings.'
			},
			{
				'imgUrl': 'img/denys-nevozhai-191635-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'A beautiful landscape!',
				'description': 'A mountain is generally steeper than a hill.'
			},
			{
				'imgUrl': 'img/matthew-henry-20172-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'What you think?',
				'description': 'This is very nice photo'
			},
			{
				'imgUrl': 'img/roya-ann-miller-110092-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'What you think?',
				'description': 'This is very nice photo'
			},
			{
				'imgUrl': 'img/averie-woodard-111822-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'Ye-ye',
				'description': 'This is very nice photo'
			},
			{
				'imgUrl': 'img/toa-heftiba-106427-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'Very nice coffee',
				'description': 'This is very nice photo'
			},
			{
				'imgUrl': 'img/syd-wachs-120737-unsplash.jpg',
				'url': 'http://example.com/',
				'name': 'Books!',
				'description': 'This is very nice photo'
			},
		]
	};

	const fireUpSlider = (settings = sliderSettings, data = imgs) => {

		const elMikeSlider = document.querySelector('#jsMikeSlider');

		//basic arguments check
		if (data === undefined || settings === undefined || !elMikeSlider) {
			return false;
		}
		// display title of slider?
		if (settings.displayTitle) {
			const newElTitle = document.createElement('h1');
			newElTitle.classList.add('section__title');
			newElTitle.textContent = data.title;
			elMikeSlider.appendChild(newElTitle);
		}
		// create section slideshow
		const newElSlideShow = document.createElement('div');
		newElSlideShow.classList.add('section__slideshow');
		elMikeSlider.appendChild(newElSlideShow);
		
		// create slideshow container
		const newElSlideshowContainer = document.createElement('div');
		newElSlideshowContainer.classList.add('slideshow__container');
		newElSlideShow.appendChild(newElSlideshowContainer);

		// settings and data
		const {imagesPerSlide} = settings;
		const {moveStep} = settings;
		const {images} = data;
		const numberOfSlides = Math.ceil(images.length/imagesPerSlide);
		let xMove = 0;

		// 
		const switchSlide = (where) => {
			console.log(`triggered || where: ${where} || xMove: ${xMove}`);
			const singleMoveValue = ((100/numberOfSlides)/imagesPerSlide);
			const limit = (-1) * ((singleMoveValue * images.length) - (singleMoveValue*imagesPerSlide));

			if (where === 'prev' && xMove < 0) {
				xMove += singleMoveValue * moveStep;
			} else if (where === 'next' && xMove > limit) {
				xMove -= singleMoveValue * moveStep;
			} else {
				return false;
			}
			newElSlideshowContainer.style.transform = `translateX(${xMove}%)`;
			return true;
		};

		const autoSlide = (time = 5000) => {
			console.log('we in');
			if (isNaN(time) || time < 0 || time > 60000) {
				return false;
			}
			console.log('we in -- >  true');
			window.setInterval(switchSlide('next'), time);
			console.log(`time is ${time}`);

		};

		// create nav buttons
		if (settings.navButtons) {
			const newElNavigation = document.createElement('div');
			newElNavigation.classList.add('section__navigation');
			elMikeSlider.appendChild(newElNavigation);

			const newElPrev = document.createElement('button');
			newElPrev.classList.add('nav-button');
			newElPrev.classList.add('nav-button__prev');
			newElPrev.textContent = settings.buttonPrev;
			newElNavigation.appendChild(newElPrev);
			newElPrev.addEventListener('click', () => {switchSlide('prev');}, false);
			const newElNext = document.createElement('button');
			newElNext.classList.add('nav-button');
			newElNext.classList.add('nav-button__next');
			newElNext.textContent = settings.buttonNext;
			newElNavigation.appendChild(newElNext);
			newElNext.addEventListener('click', () => {switchSlide('next');}, false);
		}

		const fillWithTiles = (slideContainer, startIndex) => {

			for (let i = startIndex, loopCount = 0; i < images.length && loopCount < imagesPerSlide; i++, loopCount++) {

				const newElTile = document.createElement('a');

				newElTile.classList.add('slide__tile');
				newElTile.style.flex = `${1/imagesPerSlide}`;
				slideContainer.appendChild(newElTile);
		
				for (const key in images[i]) {
					if (images[i].hasOwnProperty(key)) {

						const newElSlideUnit = document.createElement('div');
						const newElImgUrl = document.createElement('div');

						if (key !== 'url') {
							newElSlideUnit.classList.add('tile__unit');
							newElSlideUnit.classList.add(`tile__${key}`);
							newElImgUrl.classList.add('imgUrl__inner');
						}
						switch (key) {
						case 'name':
							newElSlideUnit.textContent = images[i][key];
							break;
						case 'description':
							newElSlideUnit.textContent = images[i][key];
							break;
						case 'imgUrl':
							newElImgUrl.style.backgroundImage = `url(${images[i][key]})`;
							newElSlideUnit.appendChild(newElImgUrl);
							break;
						case 'url' :
							newElTile.setAttribute('href',`${images[i][key]}`);
						}
						newElTile.appendChild(newElSlideUnit);


					}
				}
			}
		};
		const fillWithSlides = () => {

			let newElsSingleSlide = [];
			let slideNo = 0;
			let imgStartIndex = 0;

			for (let i = 0; i < numberOfSlides; i++) {
	
				newElsSingleSlide[slideNo] = document.createElement('div');
				newElsSingleSlide[slideNo].classList.add('slideshow__slide');
				newElsSingleSlide[slideNo].setAttribute('data-slide-no', slideNo);
				newElsSingleSlide[slideNo].style.flex = `${1/(numberOfSlides)}`;
				
				fillWithTiles(newElsSingleSlide[slideNo], imgStartIndex);

				newElSlideshowContainer.appendChild(newElsSingleSlide[slideNo]);
				imgStartIndex += imagesPerSlide;
				slideNo++;
			}
			newElSlideshowContainer.style.width = `${numberOfSlides* 100}%`;
		};

		fillWithSlides();
		return true;
	};
	fireUpSlider();

};

(function(){

	mikeSlider();

})();